package com.steven.foursquare

import android.app.Application
import android.util.Log

class FoursquareApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Log.i("FoursquareApp","App created")
    }
}

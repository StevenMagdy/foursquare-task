@file:JvmName("Utils")

package com.steven.foursquare

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .connectTimeout(10, TimeUnit.SECONDS)
        .build()
}

package com.steven.foursquare

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.steven.foursquare.VenuesListAdapter.VenuesVH
import com.steven.foursquare.model.FSVenue
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_venue.*


class VenuesListAdapter(
  private var venues: List<FSVenue> = emptyList()
) : RecyclerView.Adapter<VenuesVH>() {

  private class Differ(
    private val oldList: List<FSVenue>,
    private val newList: List<FSVenue>
  ) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
      oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
      oldList[oldItemPosition] == newList[newItemPosition]
  }

  override fun getItemCount() = venues.size

  fun setItems(newVenues: List<FSVenue>) {
    val result = DiffUtil.calculateDiff(Differ(venues, newVenues), true)
    venues = newVenues
    result.dispatchUpdatesTo(this)
  }

  override fun onBindViewHolder(holder: VenuesVH, position: Int) {
    holder.setup(venues[position])
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenuesVH {
    return VenuesVH(
      LayoutInflater
        .from(parent.context)
        .inflate(R.layout.item_venue, parent, false)
    )
  }

  class VenuesVH(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun setup(venue: FSVenue) {
      venueTitle.text = venue.name.trim()
      venueDesc.text = venue.location.address?.trim() ?: ""
      venue.allPhotos?.firstOrNull()?.let {
        GlideApp.with(containerView)
          .load(it.url)
          .apply(requestOptions)
          .into(venueImg)
      }
    }

    companion object {
      val requestOptions by lazy {
        RequestOptions().apply {
          transform(CenterCrop(), RoundedCorners(8))
        }
      }
    }
  }
}

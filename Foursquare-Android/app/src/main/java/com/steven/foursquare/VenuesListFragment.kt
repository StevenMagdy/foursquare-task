package com.steven.foursquare

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.steven.foursquare.VenuesListVM.ListStatus
import com.steven.foursquare.VenuesListVM.UpdateMode
import com.steven.foursquare.model.FSVenue
import kotlinx.android.synthetic.main.fragment_list_venues.*

class VenuesListFragment : Fragment() {

  val adapter by lazy { VenuesListAdapter() }

  private val vm by viewModels<VenuesListVM>()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_list_venues, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setHasOptionsMenu(true)
    venuesList.adapter = adapter
    venuesList.layoutManager = LinearLayoutManager(context)

    vm.state.observe(this, Observer {
      handleStatusChanges(it)
    })
    venuesRefreshLayout.setOnRefreshListener {
      vm.reload(true)
    }
    checkAndRequestPermission()
  }

  private fun checkAndRequestPermission() {
    if (activity == null) return
    val permission = Manifest.permission.ACCESS_FINE_LOCATION
    if (ContextCompat.checkSelfPermission(
        activity!!,
        permission
      ) == PackageManager.PERMISSION_GRANTED
    ) {
      vm.startUpdating()
      return
    }
//    if (ActivityCompat.shouldShowRequestPermissionRationale(
//        activity!!,
//        permission
//      )
//    ) {
//    }
    requestPermissions(
      arrayOf(permission),
      111
    )
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.venues_mode, menu)
    val item = menu.findItem(R.id.venuesModeMenu)
    vm.mode.value?.let {
      val otherMode =
        if (it == UpdateMode.Realtime) UpdateMode.Single
        else UpdateMode.Realtime
      item.title = otherMode.title()
    }
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (requestCode != 111) return
    if (grantResults.firstOrNull() != PackageManager.PERMISSION_GRANTED) {
      vm.state.value = ListStatus.Error
      return
    }
    vm.startUpdating()
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    if (item.itemId == R.id.venuesModeMenu) {
      val otherMode =
        if (vm.mode.value == UpdateMode.Realtime) UpdateMode.Single
        else UpdateMode.Realtime
      vm.setMode(otherMode)
      activity?.invalidateOptionsMenu()
      return true
    }
    return super.onOptionsItemSelected(item)

  }

  private fun handleStatusChanges(newStatus: ListStatus) {
    when (newStatus) {
      is ListStatus.Data -> showData(newStatus.value)
      is ListStatus.Empty -> showMessage("No data found !!")
      is ListStatus.Loading -> showLoader()
      else -> showMessage("Something went wrong !!")
    }
  }

  private fun showLoader() {
    venuesList.visibility = View.GONE
    messageTV.visibility = View.GONE
    venuesRefreshLayout.isRefreshing = true
  }

  private fun showMessage(message: String) {
    venuesList.visibility = View.GONE
    messageTV.text = message
    messageTV.visibility = View.VISIBLE
    venuesRefreshLayout.isRefreshing = false
  }

  private fun showData(value: List<FSVenue>) {
    messageTV.visibility = View.GONE
    venuesRefreshLayout.isRefreshing = false
    venuesList.visibility = View.VISIBLE
    adapter.setItems(value)
  }
}

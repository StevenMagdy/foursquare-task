package com.steven.foursquare

import android.annotation.SuppressLint
import android.app.Application
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.core.content.edit
import androidx.core.content.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.steven.foursquare.model.FSBaseResponse
import com.steven.foursquare.model.FSExploreResponse
import com.steven.foursquare.model.FSPhotosResponse
import com.steven.foursquare.model.FSVenue
import com.steven.foursquare.network.Router
import kotlinx.coroutines.launch

class VenuesListVM(application: Application) : AndroidViewModel(application), LocationListener {

  private val prefs by lazy {
    val context = getApplication<FoursquareApp>().applicationContext
    return@lazy PreferenceManager.getDefaultSharedPreferences(context)
  }
  private val locationManager by lazy {
    getApplication<FoursquareApp>().applicationContext.getSystemService<LocationManager>()!!
  }

  private val _mode = MutableLiveData<UpdateMode>().apply {
    val mode = UpdateMode.byValue(prefs.getInt("update_mode", 0))
    value = mode ?: UpdateMode.Realtime
  }

  val mode: LiveData<UpdateMode>
    get() = _mode

  val state = MutableLiveData<ListStatus>()

  private val location = MutableLiveData<Location>()

  fun reload(spinner: Boolean = false) {
    if (spinner) {
      state.value = ListStatus.Loading
    }
    if (location.value == null) {
      state.value = ListStatus.Error
      return
    }
    viewModelScope.launch {
      val response = try {
        Router.instance.venues("${location.value!!.latitude},${location.value!!.longitude}")
      } catch (e: Exception) {
        Log.e("VenuesListVM", e.toString())
        null
      }
      handleExploreResponse(response)
    }
    locationManager.toString()

  }

  fun setMode(newMode: UpdateMode) {
    prefs.edit {
      this.putInt("update_mode", newMode.rawValue)
    }
    _mode.value = newMode
    startUpdating()
  }

  @SuppressLint("MissingPermission")
  fun startUpdating() {
    if (mode.value == UpdateMode.Realtime) {
      try {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000L, 1000f, this)
      } catch (e: Exception) {
        Log.e("VenuesListVM", e.toString())
      }
    } else {
      if (location.value == null) {
        location.value = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        locationUpdated()
      }
      locationManager.removeUpdates(this)
    }
  }

  private fun locationUpdated() {
    val isEmpty = (state.value as? ListStatus.Data)?.value?.isEmpty() ?: true
    reload(isEmpty)
  }

  private fun handleExploreResponse(baseResponse: FSBaseResponse<FSExploreResponse>?) {
    when {
      baseResponse?.response?.venues == null -> state.value = ListStatus.Error
      baseResponse.response.venues!!.isEmpty() -> state.value = ListStatus.Empty
      else -> loadPhotos(baseResponse.response.venues!!)
    }
  }

  private fun loadPhotos(venues: List<FSVenue>) {
    val venues = venues.toMutableList()
    viewModelScope.launch {
      for (venue in venues.withIndex()) {
        val response = loadPhotos(venue.value)
        venues[venue.index].allPhotos = response?.response?.photos?.items ?: emptyList()
      }
      state.value = ListStatus.Data(venues)
    }
  }

  private suspend fun loadPhotos(venue: FSVenue): FSBaseResponse<FSPhotosResponse>? {
    return try {
      Router.instance.photos(venue.id)
    } catch (e: Exception) {
      Log.e("VenuesListVM", e.toString())
      null
    }
  }

  override fun onLocationChanged(location: Location?) {
    location?.let { this.location.value = it }
    locationUpdated()
  }

  override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
  }

  override fun onProviderEnabled(provider: String?) {
    startUpdating()
  }

  override fun onProviderDisabled(provider: String?) {
    state.value = ListStatus.Error
  }

  sealed class ListStatus {
    object Loading : ListStatus()
    object Error : ListStatus()
    object Empty : ListStatus()
    data class Data(val value: List<FSVenue>) : ListStatus()
  }

  enum class UpdateMode(val rawValue: Int) {
    Realtime(1) {
      override fun title() = "Realtime"
    },
    Single(2) {
      override fun title() = "Single"
    };

    abstract fun title(): String

    companion object {
      fun byValue(rawValue: Int): UpdateMode? {
        for (value in values()) {
          if (value.rawValue == rawValue) return value
        }
        return null
      }
    }
  }
}

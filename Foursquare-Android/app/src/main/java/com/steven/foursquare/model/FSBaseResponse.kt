package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSBaseResponse<R>(
    val meta: FSMeta,
    val response: R?
)

package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSExploreGroup(val type: String?,
                          val name: String?,
                          val items: List<FSExploreGroupItem>?)


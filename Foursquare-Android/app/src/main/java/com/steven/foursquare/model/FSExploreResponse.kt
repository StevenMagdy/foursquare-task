package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSExploreResponse(
    val totalResults: Int?,
    val groups: List<FSExploreGroup>?
) {
    val venues: List<FSVenue>?
        get() {
            if (groups?.firstOrNull()?.items == null) return null
            val res = mutableListOf<FSVenue>()
            for (item in groups.first().items!!) {
                item.venue?.let { res.add(it) }
            }
            return res
        }
}


package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSLocation(
    val address: String?,
    val crossStreet: String?,
    val lat: Double,
    val lng: Double,
    val distance: Double
)

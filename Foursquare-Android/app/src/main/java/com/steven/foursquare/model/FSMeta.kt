package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSMeta(
    val code: Int,
    val requestId: String
)

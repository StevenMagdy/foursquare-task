package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSPhotos(
    val count: Int?,
    val items: List<FSPhotosItem>?
)

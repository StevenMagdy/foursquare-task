package com.steven.foursquare.model

import com.squareup.moshi.JsonClass
import kotlin.math.min

@JsonClass(generateAdapter = true)
data class FSPhotosItem(
  val id: String,
  val prefix: String,
  val suffix: String,
  val width: Int,
  val height: Int
) {
  val url: String
    get() {
      val minDimension = min(width, height)
      val size = "${minDimension}x${minDimension}"
      return "${prefix}${size}${suffix}"
    }
}

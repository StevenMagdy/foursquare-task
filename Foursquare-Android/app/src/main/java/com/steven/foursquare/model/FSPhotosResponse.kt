package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSPhotosResponse(val photos: FSPhotos?)

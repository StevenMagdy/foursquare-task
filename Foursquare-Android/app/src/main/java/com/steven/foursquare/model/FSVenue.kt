package com.steven.foursquare.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FSVenue(
  val id: String,
  val name: String,
  val location: FSLocation,
  var allPhotos: List<FSPhotosItem>?
)

package com.steven.foursquare.network

import com.steven.foursquare.model.FSBaseResponse
import com.steven.foursquare.model.FSExploreResponse
import com.steven.foursquare.model.FSPhotosResponse
import com.steven.foursquare.okHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Router {
    @GET("venues/explore")
    suspend fun venues(
        @Query("ll") latLng: String,
        @Query("client_id") client: String = clientID,
        @Query("client_secret") secret: String = clientSecret,
        @Query("v") version: String = apiVersion
    ): FSBaseResponse<FSExploreResponse>

    @GET("venues/{venueId}/photos")
    suspend fun photos(
        @Path("venueId") venueId: String,
        @Query("client_id") client: String = clientID,
        @Query("client_secret") secret: String = clientSecret,
        @Query("v") version: String = apiVersion
    ): FSBaseResponse<FSPhotosResponse>

    companion object {
        val instance: Router by lazy {
            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create())
                .baseUrl("https://api.foursquare.com/v2/")
                .build()
            retrofit.create(Router::class.java)
        }
    }
}

private var clientID = "5XL5Z4MBZ23MMTEMJSKYR052I2JVUMYE0NBM5NZRUIAJB1WT"
private var clientSecret = "441SVE5I0I2M53RE5LATTKVYV2V45GPJH5SCZCZBCYRKAYRB"
private var apiVersion = "20191022"

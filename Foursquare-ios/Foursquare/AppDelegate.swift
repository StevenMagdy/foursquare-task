//
//  AppDelegate.swift
//  Foursquare
//
//  Created by Steven on 10/21/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication,
                   //swiftlint:disable:next discouraged_optional_collection
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.

    if #available(iOS 13.0, *) {
    } else {
      let delegate = UIApplication.shared.delegate as! AppDelegate
      delegate.window = UIWindow(frame: UIScreen.main.bounds)
      delegate.window!.makeKeyAndVisible()
      let nav = UINavigationController(rootViewController: R.storyboard.main.viewController()!)
      delegate.window!.rootViewController = nav
    }

    return true
  }

  // MARK: UISceneSession Lifecycle
  @available(iOS 13.0, *)
  func application(_ application: UIApplication,
                   configurationForConnecting connectingSceneSession: UISceneSession,
                   options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }

  @available(iOS 13.0, *)
  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running,
    // this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }
}

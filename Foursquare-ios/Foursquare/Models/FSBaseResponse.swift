//
//  FSBaseResponse.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Foundation

struct FSBaseResponse<Response: Codable> {
  let meta: FSMeta
  let response: Response
}

extension FSBaseResponse: Codable {
}

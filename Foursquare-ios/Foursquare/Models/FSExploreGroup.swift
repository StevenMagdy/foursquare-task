//
//  FSExploreGroup.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Foundation

struct FSExploreGroup {
  let type: String
  let name: String
  let items: [FSExploreGroupItem]
}

extension FSExploreGroup: Codable {
}

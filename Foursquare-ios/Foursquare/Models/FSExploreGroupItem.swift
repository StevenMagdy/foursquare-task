//
//  FSExploreGroupItem.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Foundation

struct FSExploreGroupItem {
  //let reasons: Reasons
  let venue: FSVenue
  let referralId: String
}

extension FSExploreGroupItem: Codable {
}

//
//  FSExploreResonse.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

struct FSExploreResonse {
//  let suggestedFilters: SuggestedFilters
//  let headerLocation: String
//  let headerFullLocation: String
//  let headerLocationGranularity: String
  let totalResults: Int
//  let suggestedBounds: SuggestedBounds
  let groups: [FSExploreGroup]
}

extension FSExploreResonse {

  var allVenues: [FSVenue] {
    var res: [FSVenue] = []
    for group in groups {
      for item in group.items {
        res.append(item.venue)
      }
    }
    return res
  }

  var firstGroupVenues: [FSVenue] {
    guard let firstGroup = groups.first else { return [] }
    var res: [FSVenue] = []
    firstGroup.items.forEach { res.append($0.venue) }
    return res
  }
}

extension FSExploreResonse: Codable {
}

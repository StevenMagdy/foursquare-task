//
//  FSLocation.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Foundation

// MARK: - Location
struct FSLocation {
    let address: String?
    let crossStreet: String?
    let lat: Double
    let lng: Double
    //let labeledLatLngs: [LabeledLatLng]?
    let distance: Double
    //let postalCode: String?
    //let cc: Cc
    //let neighborhood: String?
    //let city: City
    //let state: State
    //let country: Country
    //let formattedAddress: [String]
}

extension FSLocation: Codable {
}

extension FSLocation: Equatable {
}

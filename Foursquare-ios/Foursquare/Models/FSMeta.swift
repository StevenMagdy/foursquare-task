//
//  FSMeta.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

struct FSMeta {
  let code: Int
  let requestId: String
}

extension FSMeta: Codable {
}

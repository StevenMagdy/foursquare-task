//
//  FSPhotos.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//

struct FSPhotos {
  let count: Int?

  let items: [FSPhotosItem]?
  let dupesRemoved: Int?
}

extension FSPhotos: Codable {
}

//
//  FSPhotosItem.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Foundation

struct FSPhotosItem {
    let id: String
    let createdAt: Int
    //let source: Source
    let prefix: String
    let suffix: String
    let width: Int
    let height: Int
    //let user: User
    //let checkin: Checkin
    //let visibility: String
}

extension FSPhotosItem: Codable {
}

extension FSPhotosItem: Equatable {
}

extension FSPhotosItem {
  var url: URL {
    let minDimension = min(width, height)
    let size = "\(minDimension)x\(minDimension)"
    return URL(string: prefix)!.appendingPathComponent(size).appendingPathComponent(suffix)
  }
}

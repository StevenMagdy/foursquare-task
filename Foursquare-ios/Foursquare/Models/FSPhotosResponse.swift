//
//  FSPhotosResponse.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//

struct FSPhotosResponse {
   let photos: FSPhotos?
}

extension FSPhotosResponse: Codable {
}

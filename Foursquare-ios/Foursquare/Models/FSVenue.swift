//
//  FSVenue.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import DifferenceKit

struct FSVenue {
  let id: String
  let name: String
  let location: FSLocation
  //let categories: [Category]
  var photos: [FSPhotosItem] = []
  //let venuePage: VenuePage?
  //let delivery: Delivery?
}

extension FSVenue: Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case name
    case location
  }
}

extension FSVenue: Equatable {
}

extension FSVenue: Differentiable {
  var differenceIdentifier: String {
    return id
  }
}

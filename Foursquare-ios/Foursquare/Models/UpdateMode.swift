//
//  UpdateMode.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//

enum UpdateMode: Int {
  case single = 1
  case realtime = 2

  var title: String {
    switch self {
      case .single: return "Single"
      case .realtime: return "Realtime"
    }
  }
}

//
//  Promises.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//
import Alamofire
import PromiseKit

enum Promises {

  static func getVenues(lat: Double, lng: Double) -> Promise<[FSVenue]> {
    return Promise { seal in
      Alamofire.request(Router.venues(lat: lat, lng: lng)).responseJSON { response in
        guard let responseData = response.data else {
          return seal.reject(response.error!)
        }
        do {
          let decoded = try JSONDecoder().decode(FSBaseResponse<FSExploreResonse>.self, from: responseData)
          return seal.fulfill(decoded.response.firstGroupVenues)
        } catch {
          return seal.reject(error)
        }
      }
    }
  }

  static func getPhotos(for venues: [FSVenue]) -> [Promise<FSVenue>] {
    var res: [Promise<FSVenue>] = []
    venues.forEach { res.append(getPhotos(for: $0)) }
    return res
  }

  static func getPhotos(for venue: FSVenue) -> Promise<FSVenue> {
    return Promise { seal in
      Alamofire.request(Router.photos(venueId: venue.id)).responseJSON { response in
        guard let responseData = response.data else {
          return seal.reject(response.error!)
        }
        do {
          let decoded = try JSONDecoder().decode(FSBaseResponse<FSPhotosResponse>.self, from: responseData)
          print(decoded.meta.code)
          var org = venue
          org.photos = decoded.response.photos?.items ?? []
          return seal.fulfill(org)
        } catch {
          return seal.reject(error)
        }
      }
    }
  }
}

//
//  Router.swift
//  Foursquare
//
//  Created by Steven on 10/21/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Alamofire

private let baseURL = URL(string: "https://api.foursquare.com/v2/")!

#warning("Save Auth parameters somewhere else")
private let clientID = "5XL5Z4MBZ23MMTEMJSKYR052I2JVUMYE0NBM5NZRUIAJB1WT"
private let clientSecret = "441SVE5I0I2M53RE5LATTKVYV2V45GPJH5SCZCZBCYRKAYRB"
private let apiVersion = "20191022"

enum Router: URLRequestConvertible {
  case venues(lat: Double, lng: Double)
  case photos(venueId: String)

  var method: HTTPMethod {
    switch self {
      case .photos:
        return .get
      case .venues:
        return .get
    }
  }

  var path: URL {
    switch self {
      case .photos(let id):
        return baseURL.appendingPathComponent("venues/\(id)/photos")
      case .venues:
        return baseURL.appendingPathComponent("venues/explore")
    }
  }

  var cachePolicy: URLRequest.CachePolicy {
    switch self {
      case .photos:
        // return isConnected ? .reloadIgnoringLocalCacheData : .returnCacheDataElseLoad
      return .returnCacheDataElseLoad
      case .venues:
        // return isConnected ? .reloadIgnoringLocalCacheData : .returnCacheDataElseLoad
      return .returnCacheDataElseLoad
    }
  }

  func asURLRequest() throws -> URLRequest {
    var request = URLRequest(url: path)
    request.httpMethod = method.rawValue
    request.cachePolicy = cachePolicy
    var params = Parameters()
    switch self {
      case .photos:
        break
      case .venues(let lat, let lng):
        params["ll"] = "\(lat),\(lng)"
    }
    params["client_id"] = clientID
    params["client_secret"] = clientSecret
    params["v"] = apiVersion
    request = try URLEncoding.default.encode(request, with: params)
    return request
  }
}

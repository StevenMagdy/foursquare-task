//
//  Utils.swift
//  Foursquare
//
//  Created by Steven on 10/22/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Alamofire

var isConnected: Bool {
  NetworkReachabilityManager()?.isReachable ?? false
}

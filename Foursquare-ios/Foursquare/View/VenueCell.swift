//
//  VenueCell.swift
//  Foursquare
//
//  Created by Steven on 10/23/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import Kingfisher
import UIKit

class VenueCell: UITableViewCell {

  @IBOutlet private weak var imgView: UIImageView!
  @IBOutlet private weak var titleLbl: UILabel!
  @IBOutlet private weak var descLbl: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    titleLbl.textColor = ColorCompat.label
    descLbl.textColor = ColorCompat.secondaryLabel
  }

  func setup(with venue: FSVenue) {
    titleLbl.text = venue.name
    descLbl.text = venue.location.address ?? "Unknown address"
    if let imgUrl = venue.photos.first?.url {
      imgView.kf.indicatorType = .activity
      imgView.kf.setImage(with: imgUrl)
    }
  }
}

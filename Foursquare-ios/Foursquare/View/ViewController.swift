//
//  ViewController.swift
//  Foursquare
//
//  Created by Steven on 10/21/19.
//  Copyright © 2019 Steven. All rights reserved.
//

import CoreLocation
import DifferenceKit
import PromiseKit
import UIKit

class ViewController: UIViewController {

  var currentMode = UpdateMode(rawValue: UserDefaults.standard.integer(forKey: "update_mode")) ?? .realtime
  var venues: [FSVenue] = []
  var lastKnownLocation: CLLocation?
  let locationManager = CLLocationManager()
  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private weak var messageLbl: UILabel!
  @IBOutlet private weak var tableView: UITableView!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setupLocationManager()
    activateMode()
    refresh()
  }

  func setupLocationManager() {
    locationManager.delegate = self
    locationManager.distanceFilter = 500
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    locationAuthChanged()
  }

  func locationAuthChanged(to status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()) {
    guard status != .notDetermined else {
      locationManager.requestWhenInUseAuthorization()
      return
    }
    guard status == .authorizedWhenInUse else {
      changeStatus(status: .error)
      return
    }
    activateMode()
  }

  func setupViews() {
    view.backgroundColor = ColorCompat.systemBackground
    tableView.backgroundColor = ColorCompat.systemBackground
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Refresh",
                                                       style: .plain,
                                                       target: self,
                                                       action: #selector(refresh))
    tableView.register(R.nib.venueCell)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.estimatedRowHeight = 120
  }

  func reload(with new: [FSVenue]) {
    let changeset = StagedChangeset(source: venues, target: new)
    tableView.reload(using: changeset, with: .automatic) { [weak self] data in
      self?.venues = data
    }
  }

  @objc
  func modeTapped() {
    let otherMode: UpdateMode = currentMode == .realtime ? .single : .realtime
    UserDefaults.standard.set(otherMode.rawValue, forKey: "update_mode")
    currentMode = otherMode
    activateMode()
  }

  @objc
  func refresh() {
    changeStatus(status: .loading)
    if let location = lastKnownLocation {
      loadData(with: location)
    }
  }

  func loadData(with location: CLLocation) {
    firstly {
      Promises.getVenues(lat: location.coordinate.latitude, lng: location.coordinate.longitude)
    }.then { [weak self] data -> Promise<[FSVenue]> in
      self?.changeStatus(status: .data(data: data))
      return when(fulfilled: Promises.getPhotos(for: data))
    }.done { [weak self] data in
      self?.changeStatus(status: .data(data: data))
    }.catch { [weak self] error in
      self?.changeStatus(status: .error)
      print(error.localizedDescription)
    }
  }

  func activateMode() {
    let otherMode: UpdateMode = currentMode == .realtime ? .single : .realtime
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: otherMode.title,
                                                             style: .plain,
                                                             target: self,
                                                             action: #selector(modeTapped))
    if currentMode == .realtime {
      locationManager.startUpdatingLocation()
    } else {
      locationManager.stopUpdatingLocation()
      if let location = lastKnownLocation {
        loadData(with: location)
      } else {
        locationManager.requestLocation()
      }
    }
  }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 120
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return venues.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.venueCell, for: indexPath)!
    cell.setup(with: venues[indexPath.row])
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    defer { tableView.deselectRow(at: indexPath, animated: true) }
  }
}

extension ViewController: CLLocationManagerDelegate {

  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    locationAuthChanged(to: status)
  }

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    lastKnownLocation = locations.last
    if let location = lastKnownLocation {
      loadData(with: location)
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print(error.localizedDescription)
    lastKnownLocation = nil
    changeStatus(status: .error)
  }
}

extension ViewController {
  enum ListStatus {
    case loading
    case error
    case data(data: [FSVenue])
  }

  func changeStatus(status: ListStatus) {
    switch status {
      case .data(let data):
        tableView.isHidden = data.isEmpty
        messageLbl.isHidden = !data.isEmpty
        activityIndicator.stopAnimating()
        reload(with: data)
        if data.isEmpty { messageLbl.text = "No data found !!" }
      case .error:
        messageLbl.text = "Something went wrong !!"
        tableView.isHidden = true
        messageLbl.isHidden = false
        activityIndicator.stopAnimating()
      case .loading:
        tableView.isHidden = true
        messageLbl.isHidden = true
        activityIndicator.startAnimating()
    }
  }
}
